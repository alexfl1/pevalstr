#!/bin/bash

DIRNAME='/usr/bin/dirname'
READLINK='/bin/readlink'


#CURRENT_DIR=$(dirname $(readlink -e "$0"))
CURRENT_DIR=$($DIRNAME $($READLINK -e "$0"))

PATHS_CFG=$CURRENT_DIR'/paths.cfg'

#source paths.cfg

. $PATHS_CFG

USER=$($ID -u)

if [ $USER == 0 ]; then
	echo "Installing..."
else
	echo "Please use \"sudo\" for this script"
	echo "Resualt: Install proccess is broken"
	exit 1
fi


if ! [ -d $DST_PATH ]; then
	mkdir $DST_PATH
	echo "Created destination path"
else
	echo "ERROR: Destination path is exist already"
	echo "Resualt: Install proccess is broken"
	exit 2
fi


cp -p  $CURRENT_DIR'/'* $DST_PATH
if [ $? ]; then
	echo "Files is copyed"
else
	echo "Files is not copyed"
	echo "Resualt: Install proccess is broken"
	exit 3
fi
if ! [ -f $LINK_PATH$PROG_NAME ]; then 
	ln -s  $DST_PATH$PROG_NAME $LINK_PATH$PROG_NAME
	if [ $? ]; then
		echo "Link is created"
	else
		echo  "ERROR: Link is not created"
		echo "Resualt: Install proccess is broken"
		exit 4
	fi
else 
	echo "Link is exist"
fi

echo "Resualt: Installation is complete."
