# README #
pevalstr

### What is this repository for? ###

* Quick summary
It is comfortable wrapper for peval
* Version
1.0

### How do I get set up? ###

* Summary of set up

1. install irrtoolset
2. cd /tmp
3. git clone https://alexfl1@bitbucket.org/alexfl1/pevalstr.git
4. cd pevalstr
5. ./install.sh

### Dependencies ###
irrtoolset

### Help of this programm ###

Pevalstr help ( -h option )

It is comfortable wrapper for peval

----
Usage:

`pevalstr [a46hi] <AS_SET_NAME>|<AS_NUM>`

Options:

`-a - as only`

`-4 - ipv4 prefixes (default)`

`-6 - ipv6 prefixes`

`-h - help`

`-i - help about install peval`

Examples:
 
`pevalstr -a AS-SET-NAME`

`pevalstr -4 AS-SET-NAME`

`pevalstr -6 AS-SET-NAME`

`pevalstr AS-SET-NAME`

`pevalstr -4 AS1122`

