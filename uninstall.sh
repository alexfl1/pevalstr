#!/bin/bash

DIRNAME='/usr/bin/dirname'
READLINK='/bin/readlink'

CURRENT_DIR=$($DIRNAME $($READLINK -e "$0"))

. $CURRENT_DIR'/paths.cfg'

USER=$($ID -u)

if [ $USER == 0 ]; then
	echo "Uninstalling..."
else
	echo "Please use \"sudo\" for this script"
	echo "Resualt: Uninstall proccess is broken"
	exit 1
fi



if [ -f $LINK_PATH$PROG_NAME ]; then
	rm $LINK_PATH$PROG_NAME
	echo "Link is deleted"
else
	echo "ERROR: Link is not found"
fi

if [ -d $DST_PATH ]; then
	rm -rf $DST_PATH
	echo "Directory is deleted"
else
	echo "ERROR: Directory is not found"
	echo "Resualt: Uninstall proccess is broken"
	exit 2
fi

echo "Resualt: Uninstallation is complete"
