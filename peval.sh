

check_peval() {

if [ ! -f $PEVALPATH ]; then
	echo "ERROR: Peval is not installed."
	echo "Please install irrtoolset."
	echo "You can use key \"-i\" to print information about the installation procedure"
	exit 10
else 
	return 1
fi

}

peval_std() {
	check_peval
	$PEVALPATH $1	| $SED 's/[({})]//g' | $TR ', ' ' \n'
}

peval_v6() {
	check_peval
	$PEVALPATH "afi ipv6 $1" | $SED 's/[({})]//g' | $TR ', ' ' \n'
}

peval_as() {
	check_peval
	$PEVALPATH -no-as $1 | $SED 's/[({})]//g' | $SED 's/ $//' | $TR ' ' '\n'
}

